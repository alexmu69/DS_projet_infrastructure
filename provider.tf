terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    version = "~> 4.0" }
  }

  backend "s3" {
    bucket     = "terraform-state-project-petclinic"
    key        = "terraform.tfstate"
    region     = "eu-west-3"
    access_key = "XXXXXXXXXXXXX"
    secret_key = "XXXXXXXXXXXXX"
  }
} # Configure the AWS Provider provider “aws” { region = “eu-west-3” }

